class Story {
  int id;
  String content;
  String type;
  int userId;
  int recent;
  DateTime createdAt;
  String updatedAt;
  String createdAtTimezone;
  String updatedAtTimezone;
  String imageAddress;

  Story(
      {this.id,
      this.content,
      this.type,
      this.userId,
      this.recent,
      this.createdAt,
      this.updatedAt,
      this.createdAtTimezone,
      this.updatedAtTimezone,
      this.imageAddress});

  Story.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    type = json['type'];
    userId = json['user_id'];
    recent = json['recent'];
    createdAt = DateTime.parse(json["created_at"]);
    updatedAt = json['updated_at'];
    createdAtTimezone = json['created_at_timezone'];
    updatedAtTimezone = json['updated_at_timezone'];
    imageAddress = json['image_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['content'] = this.content;
    data['type'] = this.type;
    data['user_id'] = this.userId;
    data['recent'] = this.recent;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_at_timezone'] = this.createdAtTimezone;
    data['updated_at_timezone'] = this.updatedAtTimezone;
    data['image_address'] = this.imageAddress;
    return data;
  }
}
