

class Contents {
  int id;
  String content;
  String type;
  int contentableId;
  String contentableType;
  String createdAt;
  String updatedAt;
  String deletedAt;
  String createdAtTimezone;
  String updatedAtTimezone;
  String imageAddress;

  Contents(
      {this.id,
      this.content,
      this.type,
      this.contentableId,
      this.contentableType,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.createdAtTimezone,
      this.updatedAtTimezone,
      this.imageAddress});

  Contents.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    type = json['type'];
    contentableId = json['contentable_id'];
    contentableType = json['contentable_type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    createdAtTimezone = json['created_at_timezone'];
    updatedAtTimezone = json['updated_at_timezone'];
    imageAddress = json['image_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['content'] = this.content;
    data['type'] = this.type;
    data['contentable_id'] = this.contentableId;
    data['contentable_type'] = this.contentableType;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['created_at_timezone'] = this.createdAtTimezone;
    data['updated_at_timezone'] = this.updatedAtTimezone;
    data['image_address'] = this.imageAddress;
    return data;
  }
}