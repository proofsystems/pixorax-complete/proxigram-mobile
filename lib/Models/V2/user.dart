import 'package:proxigram/Models/V2/stories.dart';

class User {
  int id;
  String fname;
  String lname;
  String description;
  String email;
  String username;
  String emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String deletedAt;
  String image;
  String coverPhoto;
  String type;
  int followersCount;
  int followingCount;
  String imageAddress;

  int isFollowed;
  int numPost;
  List<Stories> hasStories;

  User({
    this.id,
    this.fname,
    this.lname,
    this.description,
    this.email,
    this.username,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.image,
    this.coverPhoto,
    this.type,
    this.followersCount,
    this.followingCount,
    this.imageAddress,
    this.isFollowed,
    this.numPost,
    this.hasStories,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fname = json['fname'];
    lname = json['lname'];
    description = json['description'];
    email = json['email'];
    username = json['username'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    image = json['image'];
    coverPhoto = json['cover_photo'];
    type = json['type'];
    followersCount = json['followers_count'];
    followingCount = json['following_count'];
    imageAddress = json['image_address'];
    isFollowed = json['is_followed'];
    numPost = json['num_post'];
    if (json['has_stories'] != null) {
      hasStories = new List<Stories>();
      json['has_stories'].forEach((v) {
        hasStories.add(new Stories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['description'] = this.description;
    data['email'] = this.email;
    data['username'] = this.username;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['image'] = this.image;
    data['cover_photo'] = this.coverPhoto;
    data['type'] = this.type;
    data['followers_count'] = this.followersCount;
    data['following_count'] = this.followingCount;
    data['image_address'] = this.imageAddress;

    data['is_followed'] = this.isFollowed;
    data['num_post'] = this.numPost;
    if (this.hasStories != null) {
      data['has_stories'] = this.hasStories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
