import 'package:proxigram/Models/V2/feed.dart';

class Hashtag {
  int id;
  String hashtag;
  int postsCount;
  int followersCount;
  int isFollowing;
  List<Feed> posts;

  Hashtag(
      {this.id,
      this.hashtag,
      this.postsCount,
      this.followersCount,
      this.isFollowing,
      this.posts});

  Hashtag.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    hashtag = json['hashtag'];
    postsCount = json['posts_count'];
    followersCount = json['followers_count'];
    isFollowing = json['is_following'];
    if (json['posts'] != null) {
      posts = new List<Feed>();
      json['posts'].forEach((v) {
        posts.add(new Feed.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['hashtag'] = this.hashtag;
    data['posts_count'] = this.postsCount;
    data['followers_count'] = this.followersCount;
    data['is_following'] = this.isFollowing;
    if (this.posts != null) {
      data['posts'] = this.posts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
