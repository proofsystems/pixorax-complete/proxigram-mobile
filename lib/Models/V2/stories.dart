// import 'dart:convert';

// import 'package:proxigram/Models/V2/story.dart';

// List<Stories> storiesFromJson(String str) =>
//     List<Stories>.from(json.decode(str).map((x) => Stories.fromJson(x)));

// String storiesToJson(List<Stories> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Stories {
//   int id;
//   String fname;
//   String image;
//   String lname;
//   List<Story> story;

//   Stories({this.id, this.fname, this.lname, this.image, this.story});

//   factory Stories.fromJson(Map<String, dynamic> json) {
//     return Stories(
//       id: json['id'] as int,
//       fname: json['fname'] as String,
//       lname: json['lname'] as String,
//       image: json['image'] as String,
//       story: List<Story>.from(json["story"].map((x) => Story.fromJson(x))),
//     );
//   }

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "fname": fname,
//         "fname": lname,
//         "image": image,
//         "story": List<dynamic>.from(story.map((x) => x.toJson())),
//       };
// }
import 'package:proxigram/Models/V2/story.dart';

class Stories {
  int id;
  String fname;
  String lname;
  String description;
  String email;
  String username;
  String createdAt;
  String updatedAt;
  String image;
  String coverPhoto;
  int countryId;
  String type;
  int followersCount;
  int followingCount;
  String imageAddress;
  Null socialAccount;
  int isFollowed;
  int numPost;
  List<Story> story;
  Null linkedSocialAccounts;

  Stories(
      {this.id,
      this.fname,
      this.lname,
      this.description,
      this.email,
      this.username,
      this.createdAt,
      this.updatedAt,
      this.image,
      this.coverPhoto,
      this.countryId,
      this.type,
      this.followersCount,
      this.followingCount,
      this.imageAddress,
      this.socialAccount,
      this.isFollowed,
      this.numPost,
      this.story,
      this.linkedSocialAccounts});

  Stories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fname = json['fname'];
    lname = json['lname'];
    description = json['description'];
    email = json['email'];
    username = json['username'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    image = json['image'];
    coverPhoto = json['cover_photo'];
    countryId = json['country_id'];
    type = json['type'];
    followersCount = json['followers_count'];
    followingCount = json['following_count'];
    imageAddress = json['image_address'];
    socialAccount = json['social_account'];
    isFollowed = json['is_followed'];
    numPost = json['num_post'];
    
    if (json['story'] != null) {
      story = new List<Story>();
      json['story'].forEach((v) {
        story.add(new Story.fromJson(v));
      });
    }
    linkedSocialAccounts = json['linked_social_accounts'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['description'] = this.description;
    data['email'] = this.email;
    data['username'] = this.username;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['image'] = this.image;
    data['cover_photo'] = this.coverPhoto;
    data['country_id'] = this.countryId;
    data['type'] = this.type;
    data['followers_count'] = this.followersCount;
    data['following_count'] = this.followingCount;
    data['image_address'] = this.imageAddress;
    data['social_account'] = this.socialAccount;
    data['is_followed'] = this.isFollowed;
    data['num_post'] = this.numPost;
    if (this.story != null) {
      data['story'] = this.story.map((v) => v.toJson()).toList();
    }
    data['linked_social_accounts'] = this.linkedSocialAccounts;
    return data;
  }
}