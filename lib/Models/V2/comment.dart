import 'package:proxigram/Models/V2/like.dart';
import 'package:proxigram/Models/V2/user.dart';

// class Comment {
//   int commentID;
//   int userID;
//   int postID;
//   String comment;
//   int likesCount;
//   User user;
//   List<Like> likeList;

//   Comment({
//     this.commentID,
//     this.userID,
//     this.postID,
//     this.comment,
//     this.likesCount,
//     this.user,
//     this.likeList,
//   });

//   factory Comment.fromJson(Map<String, dynamic> json) {
//     return Comment(
//       commentID: json['id'] as int,
//       userID: json['user_id'] as int,
//       postID: json['post_id'] as int,
//       comment: json['comment'] as String,
//       likesCount: json['likes_count'] as int,
//       user: User.fromJson(json['user']),
//       likeList: json['likes'].cast<Like>(),
//     );
//   }
// }
class Comment {
  int id;
  int userId;
  int postId;
  String comment;
  String createdAt;
  String updatedAt;
  String deletedAt;
  int likesCount;
  int isLiked;
  User user;
  List<Like> likes;

  Comment(
      {this.id,
      this.userId,
      this.postId,
      this.comment,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.likesCount,
      this.isLiked,
      this.user,
      this.likes});

  Comment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    postId = json['post_id'];
    comment = json['comment'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    likesCount = json['likes_count'];
    isLiked = json['is_liked'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    if (json['likes'] != null) {
      likes = new List<Like>();
      json['likes'].forEach((v) {
        likes.add(new Like.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['post_id'] = this.postId;
    data['comment'] = this.comment;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['likes_count'] = this.likesCount;
    data['is_liked'] = this.isLiked;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.likes != null) {
      data['likes'] = this.likes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
