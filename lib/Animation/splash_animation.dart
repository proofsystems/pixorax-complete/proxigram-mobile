  import 'package:flutter/material.dart';
  import 'package:flutter_svg/flutter_svg.dart';

  class SplashAnimator extends StatefulWidget {
    final Stream<void> triggerAnimationStream;

    SplashAnimator({@required this.triggerAnimationStream});

    @override
    _SplashAnimatorState createState() => _SplashAnimatorState();
  }

  class _SplashAnimatorState extends State<SplashAnimator>
      with SingleTickerProviderStateMixin {
    AnimationController iconController;
    Animation<double> iconAnimation;

    @override
    void initState() {
      super.initState();
      final quick = const Duration(milliseconds: 800);
      final scaleTween = Tween(begin: 0.0, end: 1.0);
      iconController = AnimationController(duration: quick, vsync: this);
      iconAnimation = scaleTween.animate(
        CurvedAnimation(
          parent: iconController,
          curve: Curves.elasticOut,
        ),
      );
      iconController.addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) {
          Future.delayed(Duration(seconds: 1), () {
            iconController.animateTo(
              0.0,
              duration: const Duration(milliseconds: 800),
              curve: Curves.decelerate,
            );
          });
        }
      });

      widget.triggerAnimationStream.listen((_) {
        iconController
          ..reset()
          ..forward();
      });
    }

    @override
    void dispose() {
      iconController.dispose();
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      return ScaleTransition(
        scale: iconAnimation,
        child: Center(
          child: Container(
            width: 120,
            height: 120,
            child: Image.asset(
              "assets/app/app_icon.png",
              fit: BoxFit.contain,
            ),
          ),
        ),
      );
    }
  }
