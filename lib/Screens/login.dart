import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:proxigram/Theme/colors.dart';
import '../main.dart';
import '../Widgets/LoginWidget/login_credits_widget.dart';
import '../Widgets/LoginWidget/login_form_widget.dart';
import '../Widgets/LoginWidget/login_social_widget.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    Widget logoContainer = Container(
        height: double.infinity,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(top: 50, bottom: 50, right: 20),
                child: Image.asset(
                  'assets/app/app_icon.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                  child: Container(
                      child: Image.asset('assets/app/name.png',
                          fit: BoxFit.cover))),
            )
          ],
        ));

    return WillPopScope(
      onWillPop: () async {
        _onWillPop();
        return false;
      },
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Container(
              color: Color(colorBackground),
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Flexible(flex: 3, child: logoContainer),
                      Flexible(flex: 9, child: LoginSocialWidget()),
                      Flexible(flex: 2, child: CreditsWidget())
                    ],
                  ),
                  // Column(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   mainAxisSize: MainAxisSize.min,
                  //   children: <Widget>[LoginFormWidget()],
                  // ),
                  Align(
                      alignment: Alignment.center,
                      child: LoginFormWidget(),
                    ),
                  
                  Positioned(
                    child: Text("v 4.2.0"),
                    top: 10,
                    right: 10,
                  )
                ],
              ))),
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => SystemNavigator.pop(),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        true;
  }
}
