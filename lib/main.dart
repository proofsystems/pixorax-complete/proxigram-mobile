import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:proxigram/Animation/splash_animation.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Screens/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import './Screens/login.dart';
import './Theme/colors.dart';

void main() => runApp(MyApp());

//flutter build apk --target-platform android-arm,android-arm64 --split-per-abi
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        '/splash': (BuildContext context) => SplashScreen(),
        '/login': (BuildContext context) => LoginScreen(),
        '/home': (BuildContext context) => HomeScreen(),
      },
      theme: buildThemeData(),
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  final StreamController<void> iconStreamEvent = StreamController.broadcast();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), () {
      iconStreamEvent.sink.add(null);
    });
    Future.delayed(Duration(milliseconds: 3800), () {
      getCredential();
    });
  }

  getCredential() async {
    Constants.sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      Constants.isLoggedIn = Constants.sharedPreferences.getBool("login");
      Constants.token = Constants.sharedPreferences.getString("token");
      Constants.userFirstName = Constants.sharedPreferences.getString("first");
      Constants.userLastName = Constants.sharedPreferences.getString("last");
      Constants.userProfilePhoto = Constants.sharedPreferences.getString("pp");
      Constants.userEmail = Constants.sharedPreferences.getString("email");
      Constants.userDescription =
          Constants.sharedPreferences.getString("description");
      Constants.userID = Constants.sharedPreferences.getInt("id");
      Constants.userName = Constants.sharedPreferences.getString("username");
      Constants.numFollowers = Constants.sharedPreferences.getInt("followers");
      Constants.numFollowing = Constants.sharedPreferences.getInt("following");
      Constants.numPosts = Constants.sharedPreferences.getInt("posts");

      if (Constants.isLoggedIn != null) {
        if (Constants.isLoggedIn) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(),
              ));
        } else {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => LoginScreen(),
              ));
        }
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => LoginScreen(),
            ));
        Constants.isLoggedIn = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SplashAnimator(triggerAnimationStream: iconStreamEvent.stream));
  }
}

ThemeData buildThemeData() {
  final baseTheme = ThemeData.light();

  return baseTheme.copyWith(
    primaryColor: kprimaryColor,
    primaryColorDark: kaccentColor,
    accentColor: ktextColor,
  );
}
