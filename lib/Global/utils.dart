import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:progress_dialog/progress_dialog.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Screens/home.dart';
import 'package:proxigram/Screens/login.dart';

import 'package:proxigram/Theme/colors.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_hashtag_modal.dart';
import 'package:sweetalert/sweetalert.dart';

class Utils {
  ProgressDialog progressDialog;
  final HomeScreen home = new HomeScreen();

  //SHOW PROGRESS DIALOG
  showProgressDialog(BuildContext context, String _message) {
    progressDialog = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    progressDialog.style(
        message: _message,
        borderRadius: 10.0,
        backgroundColor: Color(colorBackground),
        progressWidget: SpinKitFadingCircle(
          color: Color(colorSecondary),
          size: 20,
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.fastOutSlowIn,
        messageTextStyle: TextStyle(
            color: Color(colorPrimary),
            fontSize: 15.0,
            fontWeight: FontWeight.w600));

    progressDialog.show();
  }

  //HIDE PROGRESS DIALOG
  hideProgressDialog(BuildContext context) {
    progressDialog = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    progressDialog.dismiss();
  }

  //GENERAL ERROR DIALOG
  errorDialog(BuildContext context, String _title, String _subtitle) {
    return SweetAlert.show(context,
        title: _title, subtitle: _subtitle, style: SweetAlertStyle.error);
  }

  //GENERAL SUCCESS DIALOG
  successDialog(BuildContext context, String _title, String _subtitle) {
    return SweetAlert.show(
      context,
      title: _title,
      subtitle: _subtitle,
      style: SweetAlertStyle.success,
    );
  }

  //SUCCESSFUL REGISTRATION
  registerSuccessDialog(BuildContext context) {
    return SweetAlert.show(context,
        title: "Account Registration",
        subtitle: "Account successfully registered!",
        style: SweetAlertStyle.success, onPress: (bool isSuccess) {
      if (isSuccess) {
        Navigator.of(context).pop();
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ));
        return false;
      } else {
        return true;
      }
    });
  }

  //LOGOUT
  logoutDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Logout'),
        content: new Text('Are you sure you want to Logout?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              Constants.sharedPreferences.clear();
              GoogleSignIn().disconnect();
              FacebookLogin().logOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    );
  }

  //INVALID EMAIL
  invalidEmailDialog(BuildContext context) {
    return SweetAlert.show(
      context,
      subtitle: "Please provide a valid email",
    );
  }

  //INVALID PASSWORD
  invalidPasswordDialog(BuildContext context) {
    return SweetAlert.show(
      context,
      subtitle: "Please provide a password",
    );
  }

  //FILE TOO LARGE
  largeFileErrorDialog(BuildContext context) {
    return SweetAlert.show(context,
        title: "File is too large!",
        subtitle: "Please upload a file not more than 20MB.",
        style: SweetAlertStyle.confirm);
  }

  //EDIT SUCCESSFUL
  editSuccessDialog(BuildContext context, int id) {
    return SweetAlert.show(context,
        title: "EDIT PROFILE",
        subtitle: "Profile Edited Successfully!",
        style: SweetAlertStyle.success, onPress: (bool isSuccess) {
      if (isSuccess) {
        Navigator.of(context).pop();
      }

      return true;
    });
  }

  //TIME DIFFERENCE
  getNotificationDate(DateTime date) {
    String dateString = date.toString() + "Z";
    final dateTest = DateTime.parse(dateString);

    final convertedDate = dateTest.toLocal();

    final dateNow = DateTime.now();

    final difference = dateNow.difference(convertedDate);

    return getDurationFormat(difference);
  }

  getDurationFormat(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "$n";
    }

    if (duration.inDays == 0 &&
        duration.inHours.remainder(60) == 0 &&
        duration.inMinutes.remainder(60) == 0 &&
        duration.inSeconds.remainder(60) == 0) {
      return Text("Just Now");
    } else if (duration.inDays == 0 &&
        duration.inHours.remainder(60) == 0 &&
        duration.inMinutes.remainder(60) == 0 &&
        duration.inSeconds.remainder(60) != 0) {
      if (duration.inSeconds.remainder(60) == 1) {
        return Text(
          "${duration.inSeconds.remainder(60)} second ago",
          style: TextStyle(fontSize: 15, color: Color(colorDisabled)),
        );
      } else {
        return Text("${duration.inSeconds.remainder(60)} seconds ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      }
    } else if (duration.inDays == 0 &&
        duration.inHours.remainder(60) == 0 &&
        duration.inMinutes.remainder(60) != 0 &&
        ((duration.inSeconds.remainder(60) != 0) ||
            duration.inSeconds.remainder(60) == 0)) {
      if (duration.inMinutes.remainder(60) == 1) {
        return Text("${duration.inMinutes.remainder(60)} minute ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      } else {
        return Text("${duration.inMinutes.remainder(60)} minutes ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      }
    } else if (duration.inDays == 0 &&
        duration.inHours.remainder(60) != 0 &&
        (duration.inMinutes.remainder(60) != 0 ||
            duration.inMinutes.remainder(60) == 0) &&
        (duration.inSeconds.remainder(60) != 0 ||
            duration.inSeconds.remainder(60) == 0)) {
      if (duration.inHours.remainder(60) == 1) {
        return Text("${duration.inHours.remainder(60)} hour ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      } else {
        return Text("${duration.inHours.remainder(60)} hours ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      }
    } else {
      if (duration.inDays.remainder(60) == 1) {
        return Text("${duration.inDays} day ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      } else {
        return Text("${duration.inDays} days ago",
            style: TextStyle(fontSize: 15, color: Color(colorDisabled)));
      }
    }
  }

  //CONVERT HASHTAG
  RichText convertHashtag(String text, String user, BuildContext context) {
    List<String> split = text.split(RegExp("#"));
    List<String> hashtags = split.getRange(1, split.length).fold([], (t, e) {
      var texts = e.split(" ");
      if (texts.length > 1) {
        return List.from(t)
          ..addAll(["#${texts.first}", "${e.substring(texts.first.length)}"]);
      }
      return List.from(t)..add("#${texts.first}");
    });
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: user + " ",
            style: TextStyle(
                color: Color(colorPrimary), fontWeight: FontWeight.bold),
          ),
          TextSpan(
            text: split.first,
            style: TextStyle(color: Color(colorBlack), fontSize: 12),
          )
        ]..addAll(hashtags
            .map((text) => text.contains("#")
                ? TextSpan(
                    text: text,
                    style: TextStyle(color: Colors.blue, fontSize: 12),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        showHashtagProfile(text, context);
                      },
                  )
                : TextSpan(
                    text: text,
                    style: TextStyle(color: Color(colorBlack), fontSize: 12)))
            .toList()),
      ),
    );
  }

  void showHashtagProfile(String hashtag, BuildContext context) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      backgroundColor: Color(colorBackground),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Color(colorBackground),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ViewHashtagModal(
            hashtag: hashtag,
          ),
        );
      },
    );
  }
}
