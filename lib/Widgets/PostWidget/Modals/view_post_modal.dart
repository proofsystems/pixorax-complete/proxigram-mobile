import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:photo_view/photo_view.dart';
import 'package:proxigram/Theme/colors.dart';

class ViewPostModal extends StatelessWidget {
  const ViewPostModal({
    this.imageProvider,
  });

  final ImageProvider imageProvider;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.black,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 20, top: 30),
                child: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.angleLeft,
                          color: Color(colorBackground),
                        ),
                        Text(
                          "Back",
                          style: TextStyle(
                            color: Color(colorBackground),
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ))),
            Expanded(
                child: Container(
              constraints: BoxConstraints.expand(
                height: MediaQuery.of(context).size.height,
              ),
              child: PhotoView(
                imageProvider: imageProvider,
                heroAttributes: PhotoViewHeroAttributes(tag: "hero"),
              ),
            )),
          ],
        ));
  }
}
