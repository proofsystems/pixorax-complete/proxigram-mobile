import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';

class AddCommentModal extends StatelessWidget {
  final ValueChanged<String> onPost;
  AddCommentModal({@required this.onPost});

  final commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    
    return Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: <Widget>[
            Container(
              width: 50,
              height: 50,
              child: ClipRRect(
                  borderRadius: new BorderRadius.circular(50),
                  child: Constants.userProfilePhoto == null
                      ? Container(
                          color: Color(colorPrimary),
                          alignment: Alignment.center,
                          child: Text(
                            Constants.userFirstName[0].toUpperCase(),
                            style: TextStyle(
                                color: Color(colorText), fontSize: 20),
                          ),
                        )
                      : CachedNetworkImage(
                          imageUrl:
                              "${APIServices.httpDomain}${Constants.userProfilePhoto}",
                          placeholder: (context, url) =>
                              Image.asset('assets/app/app_icon.png'),
                          errorWidget: (context, url, error) =>
                              new Icon(Icons.error),
                          fit: BoxFit.cover,
                        )),
            ),
            Expanded(
                child: Container(
              margin: EdgeInsets.only(
                left: 10,
              ),
              child: TextField(
                controller: commentController,
                autofocus: false,
                decoration: new InputDecoration(
                  suffixIcon: IconButton(
                      onPressed: () => onPost(commentController.text),
                      icon: Icon(
                        FontAwesomeIcons.paperPlane,
                        size: 15,
                        color: Theme.of(context).primaryColor,
                      )),
                  contentPadding: EdgeInsets.all(10),
                  hintText: "Add a comment ...",
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                  ),
                ),
                keyboardType: TextInputType.text,
                style: new TextStyle(fontFamily: "Poppins", fontSize: 15),
              ),
            ))
          ],
        ));
  }
}
