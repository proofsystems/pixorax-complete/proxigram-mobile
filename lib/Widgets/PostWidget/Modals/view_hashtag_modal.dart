import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Models/V2/hashtag.dart';
import 'package:proxigram/Widgets/EditProfileWidget/edit_profile_widget.dart';
import 'package:proxigram/Widgets/FullPostWidget/full_post_widget.dart';
import 'package:proxigram/Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_post_modal.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:http/http.dart' as http;
import 'package:video_player/video_player.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class ViewHashtagModal extends StatefulWidget {
  final String hashtag;

  ViewHashtagModal({
    this.hashtag,
  });
  _ViewHashtagModalState createState() => _ViewHashtagModalState();
}

class _ViewHashtagModalState extends State<ViewHashtagModal> {
  String imageType;
  static String defaultProfilePhoto = "";
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;
  String imageformat = '';
  Hashtag hashtagList;
  String thumbnail = "";

  Future<Hashtag> getHashtag;

  @override
  void initState() {
    super.initState();

    getHashtag = getHashtagPost();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<Hashtag> getHashtagPost() async {
    print(APIServices.httpDomain +
        APIServices.viewHashtag +
        widget.hashtag.substring(1));
    var res = await http.get(
        Uri.encodeFull(APIServices.httpDomain +
            APIServices.viewHashtag +
            widget.hashtag.substring(1)),
        headers: {"Authorization": "Bearer " + Constants.token});

    if (res.statusCode == 200) {
      var data = json.decode(res.body);

      hashtagList = Hashtag.fromJson(data);
      print(hashtagList);
    }
    print(res.statusCode);
    return hashtagList;
  }

  Future getVideoThumbnail(String videoPathUrl) async {
    thumbnail = await VideoThumbnail.thumbnailFile(
      video: videoPathUrl,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG, // the original resolution of the video
      quality: 75,
    );

    return File(thumbnail);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color(colorBackground),
          child: Column(
            children: <Widget>[
              backButtonWidget(),
              FutureBuilder(
                  future: getHashtag,
                  builder: (context, snapshot) {
                    return snapshot.data != null
                        ? hashtagPost(snapshot.data)
                        : Center(child: CircularProgressIndicator());
                  })
            ],
          )),
    );
  }

  Widget hashtagPost(Hashtag hashtag) {
    return Column(
      children: <Widget>[
        Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        width: 1, color: Color(colorText).withOpacity(0.2)))),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                profileImageWidget(hashtag
                    .posts[hashtag.posts.length - 1].contents[0].content),
                profileStatusWidget(
                    hashtag.postsCount, hashtag.isFollowing, hashtag.id),
              ],
            )),
        hashtagPostWidget(hashtag)
      ],
    );
  }

  Widget profileStatusWidget(int postCount, int isFollowed, int hashtagID) {
    return Expanded(
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text(postCount.toString(),
                        style: TextStyle(
                            color: Color(colorBlack),
                            fontWeight: FontWeight.w700,
                            fontSize: 15)),
                  ),
                  Container(
                      child: Text("posts",
                          style: TextStyle(
                            color: Color(colorDisabled),
                          )),
                      margin: EdgeInsets.only(left: 10)),
                ],
              )),
            ],
          ),
          Container(
              margin: EdgeInsets.only(top: 10),
              child: RaisedButton(
                  padding: EdgeInsets.only(left: 50, right: 50),
                  color: hashtagList.isFollowing == 1
                      ? Color(colorTertiary)
                      : Color(colorPrimary),
                  child: Text(
                    hashtagList.isFollowing == 1 ? "following" : "follow",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    followHashtag();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)))),
        ],
      )),
    );
  }

  Widget profileImageWidget(String image) {
    return Container(
      margin: EdgeInsets.all(20),
      height: 100,
      width: 100,
      child: Stack(
        children: <Widget>[
          GradientStroke(
            strokeWidth: 3,
            radius: 50,
            gradient: LinearGradient(
                colors: [Color(colorSecondary), Color(colorTertiary)]),
            child: Container(
              padding: EdgeInsets.all(5),
              height: 95,
              width: 95,
              child: getImageType(image) == "image"
                  ? ClipRRect(
                      borderRadius: new BorderRadius.circular(50),
                      child: Container(
                          color: Color(colorBackground),
                          child: CachedNetworkImage(
                            imageUrl: "${APIServices.httpDomain}$image",
                            placeholder: (context, url) =>
                                Image.asset('assets/app/app_icon.png'),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.cover,
                          )))
                  : Container(
                      color: Color(colorBackgroundDarker),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: FutureBuilder(
                        future: getVideoThumbnail(
                            "${APIServices.httpDomain}$image"),
                        builder: (context, snapshot) {
                          return snapshot.data != null
                              ? Image.file(
                                  snapshot.data,
                                  fit: BoxFit.cover,
                                )
                              : Center(child: CircularProgressIndicator());
                        },
                      )),
            ),
            onPressed: () {},
          ),
          Align(
              alignment: Alignment.bottomRight,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Color(colorBackground),
                ),
                margin: EdgeInsets.all(5),
                padding: EdgeInsets.all(4),
                height: 40,
                width: 40,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Color(colorTertiary), Color(colorStatus)],
                      ),
                      color: Colors.white),
                  child: Center(
                    child: Icon(
                      FontAwesomeIcons.hashtag,
                      size: 15,
                      color: Colors.white,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Widget backButtonWidget() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Row(
        children: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.arrowAltCircleLeft,
              size: 25,
              color: Color(colorPrimary),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          Container(
            child: Text(
              widget.hashtag,
              style: TextStyle(
                  fontSize: 15,
                  fontStyle: FontStyle.italic,
                  fontFamily: 'Poppins'),
            ),
          )
        ],
      ),
    );
  }

  Widget hashtagPostWidget(Hashtag hashtag) {
    return GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 3,
        children: List.generate(hashtagList.posts.length, (index) {
          return hashtag.posts[index].contents.length == 1
              ? getImageType(hashtag.posts[index].contents[0].content) ==
                      "image"
                  ? GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => FullPostWidget(
                                userImage: hashtag.posts[index].user.image,
                                firstName: hashtag.posts[index].user.fname,
                                lastName: hashtag.posts[index].user.lname,
                                imageType: hashtag.posts[index].user.type,
                                isPostLiked: hashtag.posts[index].isLiked,
                                postID: hashtag.posts[index].id,
                                likesCount: hashtag.posts[index].likesCount,
                                commentList: hashtag.posts[index].comments,
                                createdAt: hashtag.posts[index].createdAt,
                                caption: hashtag.posts[index].caption,
                                content: hashtag.posts[index].contents,
                              ),
                            ));
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          padding: EdgeInsets.all(2),
                          child: ClipRRect(
                              borderRadius:
                                  new BorderRadius.all(Radius.circular(0)),
                              child: CachedNetworkImage(
                                imageUrl:
                                    "${APIServices.httpDomain}${hashtag.posts[index].contents[0].content}",
                                placeholder: (context, url) =>
                                    Image.asset('assets/app/app_icon.png'),
                                errorWidget: (context, url, error) =>
                                    new Icon(Icons.error),
                                fit: BoxFit.cover,
                              ))))
                  : GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => FullPostWidget(
                                userImage: hashtag.posts[index].user.image,
                                firstName: hashtag.posts[index].user.fname,
                                lastName: hashtag.posts[index].user.lname,
                                imageType: hashtag.posts[index].user.type,
                                isPostLiked: hashtag.posts[index].isLiked,
                                postID: hashtag.posts[index].id,
                                likesCount: hashtag.posts[index].likesCount,
                                commentList: hashtag.posts[index].comments,
                                createdAt: hashtag.posts[index].createdAt,
                                caption: hashtag.posts[index].caption,
                                content: hashtag.posts[index].contents,
                              ),
                            ));
                      },
                      child: Stack(children: <Widget>[
                        Container(
                            color: Color(colorBackgroundDarker),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            child: FutureBuilder(
                              future: getVideoThumbnail(
                                  "${APIServices.httpDomain}${hashtag.posts[index].contents[0].content}"),
                              builder: (context, snapshot) {
                                return snapshot.data != null
                                    ? Image.file(
                                        snapshot.data,
                                        fit: BoxFit.cover,
                                      )
                                    : Center(
                                        child: CircularProgressIndicator());
                              },
                            )),
                        Container(
                          margin: EdgeInsets.all(5),
                          alignment: Alignment.topRight,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Icon(
                            Icons.videocam,
                            size: 30.0,
                            color: Color(colorProxiWall),
                          ),
                        )
                      ]))
              : GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FullPostWidget(
                            userImage: hashtag.posts[index].user.image,
                            firstName: hashtag.posts[index].user.fname,
                            lastName: hashtag.posts[index].user.lname,
                            imageType: hashtag.posts[index].user.type,
                            isPostLiked: hashtag.posts[index].isLiked,
                            postID: hashtag.posts[index].id,
                            likesCount: hashtag.posts[index].likesCount,
                            commentList: hashtag.posts[index].comments,
                            createdAt: hashtag.posts[index].createdAt,
                            caption: hashtag.posts[index].caption,
                            content: hashtag.posts[index].contents,
                          ),
                        ));
                  },
                  child: Stack(children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        padding: EdgeInsets.all(2),
                        child: ClipRRect(
                            borderRadius:
                                new BorderRadius.all(Radius.circular(0)),
                            child: CachedNetworkImage(
                              imageUrl:
                                  "${APIServices.httpDomain}${hashtag.posts[index].contents[0].content}",
                              placeholder: (context, url) =>
                                  Image.asset('assets/app/app_icon.png'),
                              errorWidget: (context, url, error) =>
                                  new Icon(Icons.error),
                              fit: BoxFit.cover,
                            ))),
                    Container(
                      margin: EdgeInsets.all(5),
                      alignment: Alignment.topRight,
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: Icon(
                        Icons.collections,
                        size: 30.0,
                        color: Color(colorProxiWall),
                      ),
                    )
                  ]));
        }));
  }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }

  followHashtag() {
    setState(() {
      hashtagList.isFollowing == 1
          ? hashtagList.isFollowing = 0
          : hashtagList.isFollowing = 1;
    });
    APIServices.hashtagFollow(hashtagList.id);
  }
}
