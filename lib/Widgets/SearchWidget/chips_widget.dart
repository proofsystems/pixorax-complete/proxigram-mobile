import 'package:flutter/material.dart';
import 'package:proxigram/Theme/colors.dart';

class SearchChipsWidget extends StatefulWidget {
  final String chipName;

  SearchChipsWidget({Key key, this.chipName}) : super(key: key);
  @override
  _SearchChipsWidgetState createState() => _SearchChipsWidgetState();
}

class _SearchChipsWidgetState extends State<SearchChipsWidget> {
  bool isChipSelected = false;
  @override
  Widget build(BuildContext context) {
    return FilterChip(
      label: Text(widget.chipName),
      labelStyle: TextStyle(
          color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
      selected: isChipSelected,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      onSelected: (isSelected) {
        setState(() {
          isChipSelected = isSelected;
        });
      },
      selectedColor: Color(colorPrimary),
    );
  }
}
