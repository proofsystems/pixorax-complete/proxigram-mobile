import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Screens/login.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;

class LoginSocialWidget extends StatefulWidget {
  LoginSocialWidget({Key key}) : super(key: key);

  _LoginSocialWidgetState createState() => _LoginSocialWidgetState();
}

class _LoginSocialWidgetState extends State<LoginSocialWidget> {
  GoogleSignInAccount currentAccount;
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
      'profile',
    ],
  );
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Row(
        children: <Widget>[
          Flexible(
            flex: 3,
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0)),
                color: Color(colorSecondary),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.white, fontSize: 22),
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "or login using",
                            style: TextStyle(color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                IconButton(
                                  onPressed: () {
                                    googleSignIn();
                                  },
                                  icon: Image.asset("assets/google_icon.png"),
                                ),
                                IconButton(
                                  onPressed: () {
                                    facebookLogin();
                                  },
                                  icon: Image.asset("assets/facebook_icon.png"),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Flexible(
            flex: 5,
            child: Container(
              width: double.infinity,
              color: Colors.transparent,
              alignment: Alignment.bottomCenter,
            ),
          ),
        ],
      ),
    );
  }

  Future<void> googleSignIn() async {
    try {
      await _googleSignIn.signIn().then((result) {
        result.authentication.then((googleKey) {
          print(googleKey.accessToken);
          // print(_googleSignIn.currentUser.displayName);
          APIServices.socialLogin(context, googleKey.accessToken, "google");
        }).catchError((err) {
          print('inner error');
        });
      }).catchError((err) {
        print('error occured');
      });
    } catch (error) {
      print(error);
    }
  }

  Future<void> googleSignOut() async {
    try {
      await _googleSignIn.disconnect().then((result) {
        Constants.sharedPreferences.clear();

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }).catchError((err) {
        print('error occured');
      });
    } catch (error) {
      print(error);
    }
  }

  Future<void> facebookLogin() async {
    final result = await FacebookLogin().logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        print(token);
        APIServices.socialLogin(context, token, "FACEBOOK");
        break;

      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.error:
        break;
    }
  }

  Future<void> facebookSignOut() async {
    try {
      await FacebookLogin().logOut().then((result) {
        Constants.sharedPreferences.clear();

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }).catchError((err) {
        print('error occured');
      });
    } catch (error) {
      print(error);
    }
  }
}
