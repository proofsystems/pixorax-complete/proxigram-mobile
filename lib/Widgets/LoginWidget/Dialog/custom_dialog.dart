import 'package:flutter/material.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';

class CustomDialog extends StatelessWidget {
  final emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Container(
        padding: EdgeInsets.all(20),
        child: dialogContent(context),
      ),
    );
  }

  dialogContent(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          "Enter your email and we'll send a link to reset your password",
          textAlign: TextAlign.center,
        ),
        TextField(
          decoration: new InputDecoration(
              hintText: "Enter your email",
              contentPadding: EdgeInsets.symmetric(vertical: 5)),
          scrollPadding: EdgeInsets.all(10),
          textAlign: TextAlign.center,
          keyboardType: TextInputType.emailAddress,
          controller: emailController,
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: RaisedButton(
              color: Color(colorPrimary),
              child: Text(
                "SEND",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                APIServices.passwordReset(context, emailController.text);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0))),
        ),
      ],
    );
  }
}
