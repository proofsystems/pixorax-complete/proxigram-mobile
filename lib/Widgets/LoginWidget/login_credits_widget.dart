import 'package:flutter/material.dart';

class CreditsWidget extends StatefulWidget {
  CreditsWidget({Key key}) : super(key: key);

  _CreditsWidgetState createState() => _CreditsWidgetState();
}

class _CreditsWidgetState extends State<CreditsWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "developed by",
                style: TextStyle(
                    color: Theme.of(context).accentColor, fontSize: 12),
              )),
          Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "Gen Claude Dubouzet",
                style: TextStyle(
                    color: Theme.of(context).accentColor, fontSize: 12),
              )),
        ],
      ),
    );
  }
}
