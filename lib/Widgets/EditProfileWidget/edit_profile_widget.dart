import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/image.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:image/image.dart' as imageLib;
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;

class EditProfileWidget extends StatefulWidget {
  final String firstName;
  final String lastName;
  final String image;
  final String description;

  EditProfileWidget({
    @required this.firstName,
    @required this.lastName,
    @required this.image,
    @required this.description,
  });

  _EditProfileWidgetState createState() => _EditProfileWidgetState();
}

class _EditProfileWidgetState extends State<EditProfileWidget> {
  String defaultProfilePhoto = "";

  imageLib.Image profilePhotoUploadedImage;
  imageLib.Image coverUploadedImage;
  File profilePhoto;
  File coverPhoto;
  String profilePhotoFileName;
  String coverPhotoFileName;

  bool isProfileChanged = false;
  bool isCoverChanged = false;
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final descriptionController = TextEditingController();
  @override
  void initState() {
    defaultProfilePhoto = Constants.userFirstName[0].toUpperCase();
    profilePhoto = UserImage.imageFile;

    super.initState();
  }

  @override
  void dispose() {
    firstNameController.dispose();
    lastNameController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  updateValues() {
    setState(() {
      Constants.userFirstName = Constants.userFirstName;
      Constants.userLastName = Constants.userLastName;
      Constants.userCoverPhoto = Constants.userCoverPhoto;
      Constants.userProfilePhoto = Constants.userProfilePhoto;
      Constants.userDescription = Constants.userDescription;
    });
  }

  //UPLOAD PROFILE USING CAMERA
  Future uploadProfileFromCamera() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.camera, PermissionGroup.photos]);

    if (permissions[PermissionGroup.camera] == PermissionStatus.granted) {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);

      if (imageFile != null) {
        Utils().showProgressDialog(context, "Decoding Image...");
        compressImage(imageFile, imageFile.path);
      }
    } else {}
  }

  Future uploadProfileFromGallery() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.camera, PermissionGroup.photos]);

    if (permissions[PermissionGroup.camera] == PermissionStatus.granted) {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (imageFile != null) {
        Utils().showProgressDialog(context, "Decoding Image...");
        compressImage(imageFile, imageFile.path);
        // profilePhotoFileName = imageFile.path;
        // var capturedImage = imageLib.decodeImage(imageFile.readAsBytesSync());
        // capturedImage =
        //     imageLib.copyResize(capturedImage, width: 250, height: 250);
        // setState(() {
        //   profilePhotoUploadedImage = capturedImage;
        //   profilePhoto = imageFile;
        //   isProfileChanged = true;
        //   editUserProfile(context, imageFile);
        // });
      }
    } else {}
  }

  //UPLOAD COVER FRO GALLERY
  Future uploadCoverFromGallery() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.camera, PermissionGroup.photos]);

    if (permissions[PermissionGroup.camera] == PermissionStatus.granted) {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (imageFile != null) {
        Utils().showProgressDialog(context, "Updating Profile...");
        compressImage(imageFile, imageFile.path);
        // coverPhotoFileName = imageFile.path;
        // var capturedImage = imageLib.decodeImage(imageFile.readAsBytesSync());
        // capturedImage =
        //     imageLib.copyResize(capturedImage, width: 250, height: 250);
        // setState(() {
        //   coverUploadedImage = capturedImage;
        //   coverPhoto = imageFile;
        //   isCoverChanged = true;
        // });
      }
    } else {}
  }

  compressImage(File file, String targetPath) async {
    var compressedImage = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 50,
    );

    setState(() {
      profilePhoto = compressedImage;
      isProfileChanged = true;
      editUserProfile(context, compressedImage);
    });
  }

  Future<void> editUserInfo(BuildContext context, String fname, String lname,
      String description) async {
    Utils().showProgressDialog(context, "Editing Info...");

    String apiUrl = '${APIServices.httpDomain}${APIServices.editUser}';

    var request = http.MultipartRequest("POST", Uri.parse(apiUrl));

    if (fname.isEmpty) {
      request.fields["fname"] = Constants.userFirstName;
    } else {
      request.fields["fname"] = fname;
      Constants.userFirstName = fname;
    }
    if (lname.isEmpty) {
      request.fields["lname"] = Constants.userLastName;
    } else {
      request.fields["lname"] = lname;
      Constants.userLastName = lname;
    }

    if (description.isEmpty) {
      request.fields["description"] = "Your Grammable Description";
    } else {
      request.fields["description"] = description;
      Constants.userDescription = description;
    }

    request.headers["Authorization"] = "Bearer ${Constants.token}";

    var response = await request.send();

    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    if (response.statusCode == 200) {
      Utils().hideProgressDialog(context);
      getProfile(context, Constants.userID);
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "Edit Profile", "It's not you, it's us! Please try again.");
    }
    return response;
  }

  Future<void> editUserProfile(BuildContext context, File image) async {
   
    Utils().showProgressDialog(context, "Updating Profile Picture...");

    List<MultipartFile> newList = new List<MultipartFile>();

    String apiUrl = '${APIServices.httpDomain}${APIServices.editUser}';

    var request = http.MultipartRequest("POST", Uri.parse(apiUrl));

    request.headers["Authorization"] = "Bearer ${Constants.token}";

    var pic = await http.MultipartFile.fromPath("image", image.path);
    // var pic2 =
    //     await http.MultipartFile.fromPath("cover_photo", coverPhoto.path);
    newList.add(pic);
    // newList.add(pic2);

    //add multipart to request
    request.files.addAll(newList);
    request
        .send()
        .then((result) async {
          http.Response.fromStream(result).then((response) {
            if (response.statusCode == 200) {
              Utils().hideProgressDialog(context);

              var body = json.decode(response.body);
              setState(() {
                Constants.userProfilePhoto = body["data"]["image"].toString();
              });
            }

            return response.body;
          });
        })
        .catchError((err) => print('error : ' + err.toString()))
        .whenComplete(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: true,
        body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
                margin: EdgeInsets.only(top: 15),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      padding: EdgeInsets.all(15),
                      color: Color(colorBackgroundDarker),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Text(
                        "EDIT PROFILE",
                        style: TextStyle(color: Color(colorBackground)),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.all(10),
                        height: 120,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: <Widget>[
                            Container(
                                height: 100,
                                width: 100,
                                child: ClipRRect(
                                    borderRadius: new BorderRadius.all(
                                        Radius.circular(50)),
                                    child: isProfileChanged
                                        ? Image.file(
                                            profilePhoto,
                                            fit: BoxFit.cover,
                                          )
                                        : Constants.userProfilePhoto == null
                                            ? Container(
                                                color: Color(colorPrimary),
                                                alignment: Alignment.center,
                                                child: Text(
                                                  defaultProfilePhoto,
                                                  style: TextStyle(
                                                      color: Color(colorText),
                                                      fontSize: 20),
                                                ),
                                              )
                                            : CachedNetworkImage(
                                                imageUrl:
                                                    "${APIServices.httpDomain}${Constants.userProfilePhoto}",
                                                placeholder: (context, url) =>
                                                    Image.asset(
                                                        'assets/app/app_icon.png'),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        new Icon(Icons.error),
                                                fit: BoxFit.cover,
                                              ))),
                            Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text("Profile Photo")),
                            ),
                            Container(
                                margin: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: GestureDetector(
                                  onTap: () {
                                    uploadProfileFromGallery();
                                  },
                                  child: SvgPicture.asset(
                                      "assets/icons/gallery_icon.svg",
                                      color: Color(colorBackgroundDarker)),
                                )),
                            Container(
                                margin: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: GestureDetector(
                                  onTap: () {
                                    uploadProfileFromCamera();
                                  },
                                  child: SvgPicture.asset(
                                      "assets/icons/camera_icon.svg",
                                      color: Color(colorBackgroundDarker)),
                                )),
                          ],
                        )),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        //FIRST NAME
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(left: 20),
                              child: Text("First Name"),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    left: 20, top: 10, right: 20),
                                child: TextFormField(
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Edit First Name";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: firstNameController,
                                  decoration: new InputDecoration(
                                      contentPadding: EdgeInsets.all(10),
                                      hintText: Constants.userFirstName,
                                      fillColor: Color(colorText),
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                            new BorderRadius.circular(20.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: Color(colorPrimary)))),
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 15,
                                      color: Color(colorText)),
                                ))
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(left: 20),
                              child: Text("Last Name"),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    right: 20, left: 20, top: 10),
                                child: TextFormField(
                                  validator: (val) {
                                    val = Constants.userLastName;
                                    if (val.isEmpty) {
                                      return "Edit Last Name";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: lastNameController,
                                  decoration: new InputDecoration(
                                      contentPadding: EdgeInsets.all(10),
                                      hintText: Constants.userLastName,
                                      fillColor: Color(colorText),
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                            new BorderRadius.circular(20.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: Color(colorPrimary)))),
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 15,
                                      color: Color(colorText)),
                                ))
                          ],
                        ))
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 20),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(left: 20),
                            child: Text("Description"),
                          ),
                          Container(
                              margin:
                                  EdgeInsets.only(right: 20, left: 20, top: 10),
                              child: TextFormField(
                                validator: (val) {
                                  val = Constants.userDescription;
                                  if (val.isEmpty) {
                                    return "Add a Description";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: descriptionController,
                                decoration: new InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        top: 10,
                                        left: 10,
                                        right: 10,
                                        bottom: 50),
                                    hintText: Constants.userDescription,
                                    fillColor: Color(colorText),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(20.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        borderSide: BorderSide(
                                            width: 1,
                                            color: Color(colorPrimary)))),
                                keyboardType: TextInputType.text,
                                style: new TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 15,
                                    color: Color(colorText)),
                              )),
                          Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    margin: EdgeInsets.all(10),
                                    child: RaisedButton(
                                        child: Text(
                                          "EDIT",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        onPressed: () {
                                          editUserInfo(
                                              context,
                                              firstNameController.text,
                                              lastNameController.text,
                                              descriptionController.text);
                                        },
                                        color: Color(colorPrimary),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    30.0)))),
                                Container(
                                    margin: EdgeInsets.all(10),
                                    child: RaisedButton(
                                        child: Text(
                                          "BACK",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        color: Color(colorSecondary),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    30.0))))
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ))));
  }

  //GET PROFILE
  Future getProfile(BuildContext context, int id) async {
    final response = await http.get(
        '${APIServices.httpDomain}${APIServices.viewUser}$id',
        headers: {"Authorization": "Bearer " + Constants.token});

    if (response.statusCode == 200) {
      var resbody = json.decode(response.body);

      // If server returns an OK response, parse the JSON.
      setState(() {
        Constants.userFirstName = resbody["fname"];
        Constants.userLastName = resbody["lname"];
        Constants.userCoverPhoto = resbody["cover_photo"];
        Constants.userProfilePhoto = resbody["image"];
        Constants.userEmail = resbody["email"];
        Constants.userID = resbody["id"];
        Constants.userDescription = resbody["description"];
        Navigator.of(context).pop();
      });

      return (json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.

      throw Exception('Failed to load post');
    }
  }
}
