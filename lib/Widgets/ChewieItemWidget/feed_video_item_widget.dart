import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:video_player/video_player.dart';

class ChewieFeedItem extends StatefulWidget {
  // This will contain the URL/asset path which we want to play
  final VideoPlayerController videoPlayerController;
  final bool looping;
  final double aspectRatio;

  ChewieFeedItem({
    @required this.videoPlayerController,
    this.looping,
    @required this.aspectRatio,
    Key key,
  }) : super(key: key);

  @override
  _ChewieFeedItemState createState() => _ChewieFeedItemState();
}

class _ChewieFeedItemState extends State<ChewieFeedItem> {
  ChewieController _chewieController;
  bool isPlaying = false;
  Future<void> _future;

  @override
  void initState() {
    super.initState();
    _future = initVideoPlayer();
    // Wrapper on top of the videoPlayerController
  }

  Future<void> initVideoPlayer() async {
    await widget.videoPlayerController.initialize();
    setState(() {
      _chewieController = ChewieController(
        videoPlayerController: widget.videoPlayerController,
        aspectRatio: widget.videoPlayerController.value.aspectRatio,
        // Prepare the video to be played and display the first frame
        allowFullScreen: false,
        autoInitialize: true,
        looping: widget.looping,
        showControls: false,
        autoPlay: false,
        // Errors can occur for example when trying to play a video
        // from a non-existent URL
        errorBuilder: (context, errorMessage) {
          return Center(
            child: Text(
              errorMessage,
              style: TextStyle(color: Colors.white),
            ),
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting)
          return Center(
            child: Container(
              height: 30,
              width: 30,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white60),
              ),
            ),
          );

        return Stack(
          children: <Widget>[
            Chewie(
              controller: _chewieController,
            ),
            Container(
              alignment: Alignment.center,
              child: GestureDetector(
                onTap: () {
                  if (isPlaying) {
                    _chewieController.pause();
                    setState(() {
                      isPlaying = false;
                    });
                  } else {
                    _chewieController.play();
                    setState(() {
                      isPlaying = true;
                    });
                  }
                },
                child: Icon(
                  isPlaying ? FontAwesomeIcons.pause : FontAwesomeIcons.play,
                  size: 60,
                  color: isPlaying
                      ? Color(colorProxiWall).withOpacity(0)
                      : Color(colorProxiWall).withOpacity(0.4),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
