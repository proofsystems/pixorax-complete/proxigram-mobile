import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:proxigram/Models/V2/stories.dart';
import 'package:proxigram/Widgets/StoriesWidget/stories_images_per_user.dart';

class ViewStoryModal extends StatefulWidget {
  ViewStoryModal({
    @required this.storiesList,
    @required this.inititalIndex,
  });

  final List<Stories> storiesList;
  int inititalIndex;

  _ViewStoryModalState createState() => _ViewStoryModalState();
}

class _ViewStoryModalState extends State<ViewStoryModal> {
  int _index = 0;
  SwiperController swiperController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _index = widget.inititalIndex;

    swiperController = SwiperController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.black,
        child: Swiper(
          controller: swiperController,
          index: _index,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext c, int i) {
            return StoriesPerUser(
              storiesList: widget.storiesList,
              selectedIndex: i,
              updateFunction: callBack,
            );
          },
          itemCount: widget.storiesList.length,
          loop: false,
          duration: 500,
        ));
  }

  callBack() {
    swiperController.next();
  }
}
