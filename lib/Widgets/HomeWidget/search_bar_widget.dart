import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:proxigram/Theme/colors.dart';

class SearchbarWidget extends StatefulWidget {
  @override
  _SearchbarWidgetState createState() => _SearchbarWidgetState();
}

class _SearchbarWidgetState extends State<SearchbarWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: 1,color: Colors.white.withOpacity(0.2)))
      ),
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              width: 50,
              height: 50,
              child: GradientStroke(
                strokeWidth: 2,
                radius: 50,
                gradient: LinearGradient(
                    colors: [Color(colorPrimary), Color(colorSecondary)]),
                child: Container(
                  height: 40,
                  width: 40,
                  child: ClipRRect(
                      borderRadius: new BorderRadius.circular(50),
                      child: Image.asset(
                        "assets/pp.png",
                        fit: BoxFit.cover,
                      )),
                ),
                onPressed: () {},
              )),
          Expanded(
              child: Container(
                width: double.infinity,
            margin: EdgeInsets.only(left: 10),
            child: TextFormField(
              
              decoration: new InputDecoration(
                  suffixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        FontAwesomeIcons.search,
                        size: 15,
                        color: Theme.of(context).primaryColor,
                      )),
                  contentPadding: EdgeInsets.all(10),
                  hintText: "Search for stars",
                  
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      borderSide:
                          BorderSide(width: 1, color: Color(colorPrimary)))),
              keyboardType: TextInputType.text,
              style: new TextStyle(fontFamily: "Poppins", fontSize: 15, color: Colors.white),
            ),
          ))
        ],
      ),
    );
  }
}
